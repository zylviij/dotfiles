#!/bin/bash
url=`xclip -o`
dest="${HOME}/Videos/temp/current"
yt-dlp -o $dest -f mp4 $url | zenity --progress --pulsate --auto-close --auto-kill --text="Downloading..."

sleep 0.05

mpv $dest
rm $dest
