#!/bin/bash

apt install git
apt install python3
apt install python3-pip

# lesspass
pip3 install lesspass
echo "PATH=/home/zylviij/.local/bin:\$PATH" >> ~/.profile

# vim
apt install vim
cp ./.vimrc ~/.vimrc

