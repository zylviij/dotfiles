
set nocompatible

filetype off

set encoding=utf-8

syntax enable
colorscheme elflord

filetype plugin indent on

set hidden

set ttyfast

set backspace=eol,start,indent
set mouse=

set noexpandtab
set smarttab
set tabstop=4
set shiftwidth=4
set softtabstop=4

set nowrap

set nobackup
set noswapfile

set wildmenu

set hlsearch
set incsearch
set ignorecase
set smartcase
set showmatch

set modelines=0

set number
set ruler

set visualbell
set encoding=utf-8

set showmode
set showcmd

set cursorline
hi CursorLine cterm=bold term=bold

set list
set listchars=tab:\|\ ,space:·,nbsp:␣,trail:•,precedes:«,extends:»

set scrolloff=10

call plug#begin()
" set sensible defaults for common behaviors
Plug 'tpope/vim-sensible'

" git tools for merges, diffs, blames
Plug 'tpope/vim-fugitive'
" git tool to show git info in the side bar
Plug 'airblade/vim-gitgutter'

" file browser
Plug 'scrooloose/nerdtree'
Plug 'kien/ctrlp.vim'

" autocomplete
Plug 'ervandew/supertab'

" autoformatter
Plug 'vim-autoformat/vim-autoformat'

" linter
Plug 'dense-analysis/ale'

" airline
Plug 'vim-airline/vim-airline'
call plug#end()

map <space> <leader>

noremap <leader>= :Autoformat<CR>

noremap <leader>[ :ts<space>

noremap <leader>p :CtrlP<CR>


